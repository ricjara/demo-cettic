﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIRegister : MonoBehaviour {

    // Asignados desde editor
    public InputField usernameInput;
    public InputField passwordInput;
    public UserCreator userCreator;

    public void Register()
    {
        if (usernameInput.text.Length > 0 && passwordInput.text.Length > 0)
        {
            userCreator.Register(usernameInput.text, passwordInput.text);
        }
    }
}
