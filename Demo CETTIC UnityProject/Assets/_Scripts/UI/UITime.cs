﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UITime : MonoBehaviour {

    // Inicializar desde editor porque comienza desactivado
    public MatchController match;

    Text txt;

    void Start()
    {
        txt = GetComponent<Text>();
    }

    // Actualizar el tiempo mostrado de la ronda actual
    void Update()
    {
        txt.text = match.currentTime.ToString();
    }
}
