﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UIRetry : MonoBehaviour {

    // Reintentar nivel
	public void RetryLevel()
    {
        // Volver a cargar escena activa
        int currentSceneIndex = SceneManager.GetActiveScene().buildIndex;
        SceneManager.LoadScene(currentSceneIndex);
    }

    // Volver a menu principal
    public void ExitLevel()
    {
        SceneManager.LoadScene("Intro");
    }
}
