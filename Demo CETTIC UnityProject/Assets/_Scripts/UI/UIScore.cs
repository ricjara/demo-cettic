﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class UIScore : MonoBehaviour {

    // Inicializar desde editor porque comienza desactivado
    public MatchController match;

    Text txt;

    void Start()
    {
        txt = GetComponent<Text>();
    }

    // Actualizar el puntaje mostrado de la ronda actual
	void Update()
    {
        txt.text = match.currentScore.ToString();
    }
}
