﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UILogin : MonoBehaviour {

    // Asignados desde editor
    public InputField usernameInput;
    public InputField passwordInput;
    public UserLogin userLogin;

    public void Login()
    {
        if(usernameInput.text.Length > 0 && passwordInput.text.Length > 0)
        {
            userLogin.Login(usernameInput.text, passwordInput.text);
        }
    }
}
