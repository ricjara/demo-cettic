﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class PlayerMovement : MonoBehaviour {

    ConfigController config;
    Rigidbody2D rb;
    Animator anim;

    void Start()
    {
        // Asignar componentes
        config = GameObject.Find("GameController").GetComponent<ConfigController>();
        rb = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
    }

    // Controla el movimiento del personaje
    public void Move(Vector2 direction)
    {
        rb.velocity = direction.normalized * config.playerSpeed;

        // Rotar
        Flip();
        
        // Cambiar estado de animación
        if (direction != Vector2.zero)
            anim.SetBool("IsWalking", true);
        else
            anim.SetBool("IsWalking", false);
    }

    // Cambia la orientación del personaje según movimiento
    void Flip()
    {
        if (rb.velocity.x < 0)
        {
            transform.localScale = new Vector3(-Mathf.Abs(transform.localScale.x), transform.localScale.y, transform.localScale.z);
        }
        else if (rb.velocity.x > 0)
        {
            transform.localScale = new Vector3(Mathf.Abs(transform.localScale.x), transform.localScale.y, transform.localScale.z);
        }
    }
}
