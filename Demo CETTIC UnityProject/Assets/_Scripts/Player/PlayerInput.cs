﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(PlayerMovement))]
public class PlayerInput : MonoBehaviour {

    PlayerMovement movement;
    
	void Start () {
        // Inicializar componentes
        movement = GetComponent<PlayerMovement>();
	}
	
    public void DeactivatePlayer()
    {
        // Deshabilitar input, terminar animación de caminar y fijar velocidad en cero
        GetComponent<Animator>().SetBool("IsWalking", false);
        movement.Move(Vector2.zero);
        enabled = false;
    }

    // Actualizar con FixedUpdate por movimiento basado en física
	void FixedUpdate () {
        // Recibir input de usuario
        Vector2 inputMovement = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));

        movement.Move(inputMovement);
	}
}
