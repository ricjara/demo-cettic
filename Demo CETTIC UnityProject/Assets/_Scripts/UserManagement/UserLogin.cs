﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UserLogin : MonoBehaviour {

    public string url = "http://localhost/demo_cettic/utils/login-user.php";
    public Text responseMsg;

    public void Login(string user, string pass)
    {
        StartCoroutine(LoginUser(user, pass));
    }
    
    // Limpiar usuario de playerprefs al cargar vista con componente de login (para validación de ID)
    void Start()
    {
        PlayerPrefs.DeleteKey("userId");
    }

    IEnumerator LoginUser(string username, string password)
    {
        WWWForm formData = new WWWForm();
        formData.AddField("username", username);
        formData.AddField("password", password);

        // Enviar POST request
        WWW request = new WWW(url, formData);

        // Obtener respuesta
        yield return request;

        // Parsear datos de Json a clase auxiliar para verificar datos
        Response data = JsonUtility.FromJson<Response>(request.text);

        // Si no hay errores, almacenar ID de jugador e iniciar partida
        if(data.error == 0)
        {
            PlayerPrefs.SetInt("userId", data.userId);
            SceneManager.LoadScene("Level");
        }
        else
        {
            responseMsg.color = Color.red;
            ThrowUserError(data.error);
        }
    }

    // Método para manejar postibles errores retornados desde web
    void ThrowUserError(int error)
    {
        if (error == 1)
        {
            // Credenciales incorrectas
            responseMsg.text = "Credenciales inválidas";
        }
        else if (error == 2)
        {
            // Usuario inexistente
            responseMsg.text = "Usuario inexistente";
        }
        else if (error == 3)
        {
            // Error desconocido
            responseMsg.text = "Error desconocido";
        }
        else if (error == 4)
        {
            // Error de request
            responseMsg.text = "Datos inválidos";
        }
    }

    // Clase auxiliar para obtención de respuesta desde Json Parser
    class Response
    {
        public int error = 0;
        public int userId = 0;
    }
}
