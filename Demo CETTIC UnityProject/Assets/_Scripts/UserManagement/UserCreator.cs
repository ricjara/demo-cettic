﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UserCreator : MonoBehaviour {

    public string url = "http://localhost/demo_cettic/utils/register-user.php";
    public Text responseMsg;

    public void Register (string user, string pass) {
        StartCoroutine(CreateUser(user, pass));
	}

    IEnumerator CreateUser(string username, string password)
    {
        WWWForm formData = new WWWForm();
        formData.AddField("username", username);
        formData.AddField("password", password);

        // Enviar POST request
        WWW request = new WWW(url, formData);

        // Obtener respuesta
        yield return request;

        // Parsear datos de Json a clase auxiliar para verificar posibles errores
        Error data = JsonUtility.FromJson<Error>(request.text);

        // Ajustar color del mensaje y mostrar texto correspondiente
        if(data.error == 0)
        {
            responseMsg.color = Color.green;
            responseMsg.text = "Usuario creado con éxito";
        }
        else
        {
            responseMsg.color = Color.red;
            ThrowUserError(data.error);
        }

        
    }

    // Método para manejar postibles errores retornados desde web
    void ThrowUserError(int error)
    {
        if(error == 1)
        {
            // Usuario existente
            responseMsg.text = "Nombre de usuario ya existe";
        }
        else if(error == 2)
        {
            // Error desconocido
            responseMsg.text = "Error desconocido";
        }
        else if(error == 3)
        {
            // Error de request
            responseMsg.text = "Campos inválidos";
        }
    }

    // Clase auxiliar para obtención de errores desde Json Parser
    class Error
    {
        public int error = 0;
    }
}
