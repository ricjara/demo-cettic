﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemCollector : MonoBehaviour {

    // Detectar colisiones con el jugador, sumar puntaje y destruír
    void OnTriggerEnter2D(Collider2D other)
    {
        if(other.gameObject.name.Equals("Player"))
        {
            GameObject.Find("GameController").GetComponent<MatchController>().AddScore();
            Destroy(gameObject);
        }
    }
}
