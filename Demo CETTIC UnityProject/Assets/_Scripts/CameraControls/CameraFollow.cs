﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour {

    // Límites de la cámara
    public float minX, maxX;

    Transform player;

    void Start()
    {
        player = GameObject.Find("Player").transform;
    }


    void Update()
    {
        // Si el jugador se encuentra dentro de los límites, seguirlo en el eje X
        if(player.transform.position.x > minX && player.transform.position.x < maxX)
        {
            transform.position = new Vector3(player.transform.position.x, transform.position.y, transform.position.z);
        }
    }
}
