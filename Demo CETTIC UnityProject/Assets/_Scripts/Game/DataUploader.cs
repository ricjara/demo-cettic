﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DataUploader : MonoBehaviour {

    public string url = "http://localhost/demo_cettic/utils/save-data.php";

    public IEnumerator UploadScore(int userId, int score)
    {
        WWWForm formData = new WWWForm();
        formData.AddField("score", score);
        formData.AddField("user_id", userId);

        // Enviar POST request
        WWW request = new WWW(url, formData);

        // Obtener respuesta
        yield return request;

        // Parsear datos de Json a clase auxiliar para verificar datos
        Error data = JsonUtility.FromJson<Error>(request.text);

        ThrowError(data.error);
    }

    // Método para manejar postibles errores retornados desde web
    void ThrowError(int error)
    {
        if (error == 1)
        {
            // Error al insertar
            Debug.Log("Error desconocido");
        }
        else if (error == 2)
        {
            // Error de datos enviados
            Debug.Log("Error de request");
        }
    }

    // Clase auxiliar para obtención de errores desde Json Parser
    class Error
    {
        public int error = 0;
    }
}
