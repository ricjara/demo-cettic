﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(ConfigController))]
[RequireComponent(typeof(DataUploader))]
public class MatchController : MonoBehaviour {

    public int currentScore = 0;
    public int currentTime = 30;

    // Menu de fin de partida inicia desactivado, asignar desde editor
    public GameObject gameOverMenu;

    AudioSource audioSrc;
    ConfigController conf;

    void Start()
    {
        audioSrc = GetComponent<AudioSource>();
        conf = GetComponent<ConfigController>();

        // Inicializar el tiempo de la partida según configuración
        currentTime = conf.playTime;

        // Comenzar cuenta regresiva
        Invoke("TimeTick", 1f);
    }

    // Sumar puntaje a la partida y reproducir efecto de sonido
    public void AddScore()
    {
        currentScore += conf.itemScore;
        audioSrc.Play();
    }

    void TimeTick()
    {
        // Disminuir el tiempo restante
        currentTime--;

        if(currentTime > 0)
        {
            // Continuar pasando el tiempo cada 1 segundo
            Invoke("TimeTick", 1f);
        }
        else
        {
            // Cuando el tiempo acaba, terminar partida
            EndGame();
        }
    }

    // Ajustes para terminar la partida
    void EndGame()
    {
        // Mostrar menu de "Game Over"
        gameOverMenu.SetActive(true);

        // Desactivar entradas de jugador y detener movimiento
        GameObject player = GameObject.Find("Player");
        player.GetComponent<PlayerInput>().DeactivatePlayer();

        // Guardar datos de partida en base de datos
        int playerId = PlayerPrefs.GetInt("userId");
        DataUploader uploader = GetComponent<DataUploader>();
        StartCoroutine(uploader.UploadScore(playerId, currentScore));
    }
}
