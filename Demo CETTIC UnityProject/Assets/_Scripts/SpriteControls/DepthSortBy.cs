﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
[RequireComponent(typeof(Renderer))]
// Método que ajusta el orden de dibujo según posición en el mapa (efecto profundidad en eje Z)
public class DepthSortBy : MonoBehaviour {

    // Para que haya concordancia con la conversión de pixels por unidad
    private const int IsometricRangePerYUnit = 240;

    public bool pivotOnBase = false;
    private int adjustPivot = 0;

    [Tooltip("Utilizar para calcular orden Z")]
    public Transform target;

    [Tooltip("Utilizar para ajustar posición Z respecto al target asociado")]
    public int targetOffset = 0;

    void Update()
    {
        // Cada cuadro se ordena la profundidad de dibujo

        // Para objetos que tengan el pivote en la base (por lo general objetos altos)
        Renderer renderer = GetComponent<Renderer>();
        if (pivotOnBase)
            adjustPivot = (int)(renderer.bounds.size.y * 256f / 2f);

        if (target == null)
            target = transform;
        renderer.sortingOrder = -(int)(target.position.y * IsometricRangePerYUnit) + targetOffset - adjustPivot;
    }
}
