﻿using System.Collections;
using System;
using UnityEngine;

[RequireComponent(typeof(ConfigController))]
public class ConfigLoader : MonoBehaviour {

    // URL GET variables de configuración
    public string url = "http://localhost/demo_cettic/utils/game-config.php";

    // Inicializar desde editor porque comienza desactivado
    public MatchController mc;

    ConfigController cc;

    // Se usa IEnumerator para poder esperar el método asíncrono
    IEnumerator Start () {
        cc = GetComponent<ConfigController>();

        //Verificar si se utiliza configuración en línea
        if (cc.getOnlineConfiguration)
        {
            // Leer datos de configuración en línea
            WWW getData = new WWW(url);
            // Esperar que cargue los datos
            yield return getData;

            try
            {
                // Parsear datos de Json a clase auxiliar y pasarlos a componente de configuración
                ConfigData data = JsonUtility.FromJson<ConfigData>(getData.text);

                cc.playerSpeed = data.player_speed;
                cc.playTime = data.play_time;
                cc.itemScore = data.item_score;
            }
            catch (Exception e)
            {
                Debug.Log(e.Message);
            }
        }

        mc.enabled = true;
	}

    // Clase auxiliar para obtención de datos desde Json Parser
    class ConfigData
    {
        public float player_speed = 0;
        public int play_time = 0;
        public int item_score = 0;
    }
}
