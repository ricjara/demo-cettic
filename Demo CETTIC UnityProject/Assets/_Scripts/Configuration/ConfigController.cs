﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConfigController : MonoBehaviour {

    [Tooltip("Obtener configuración desde base de datos")]
    public bool getOnlineConfiguration = true;
    public float playerSpeed = 4.5f;
    public int playTime = 60;
    public int itemScore = 5;

}
