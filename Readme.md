# Demo CETTIC

El proyecto contenido en este repositorio es un demo que no representa un producto terminado.<br/>
Bajo ninguna circunstancia se recomienda implementar en entorno productivo.

## Notas respecto a requerimientos

Durante el desarrollo del proyecto se encontraron los siguientes comentarios o ambigüedades respecto a los requerimientos especificados.

* No se menciona el motor de base de datos a utilizar.
* No se mencionan aspectos de seguridad o validaciones.
* Se especifica que el demo debe consistir en un jugador que sigue un camino, no se especifica si debe ser un nivel pre-generado, generado procedural, o un juego de la categoría infinite runner.
* Se menciona una sección con estadísticas en el sitio web, sin embargo no se menciona si el acceso está limitado a invitados, usuarios o adminsitrador para esta sección en específico.

## Componentes principales del repositorio

### <b>Demo CETTIC UnityProject</b> - Carpeta que contiene el proyecto Unity 5.5.4

Por defecto las url para las interacciones con la base de datos se encuentran apuntando a la ruta del sitio web localhost/demo_cettic/{ script a apuntar }, en caso de no correr el servidor desde esta dirección, modificar las url desde los siguientes Script Unity (accesible desde editor):

* ConfigLoader
* DataUploader
* UserCreator
* UserLogin

### <b>Demo CETTIC Windows</b> - Carpeta que contiene la build de Windows del demo

Esta build se encuentra con las url por defecto.

### <b>demo_cettic</b> - Carpeta que contiene el sitio web y los servicios php

Dentro de esta carpeta, en config/siteconf.php se contiene la configuración de la conexión. Por defecto se encuentra configurado para correr en localhost, con nombre de usuario "demoadmin", contraseña "demo.2017" y base de datos "demo_cettic" (nombre de DB utilizado en el script de creación incluído). 

### <b>demo_cettic.sql</b> - Archivo que contiene el script de creación de la base de datos (incluye seed)

<b>IMPORTANTE</b> - Dentro del script de creación se incluye la creación de la base de datos bajo el nombre "demo_cettic", además incluye usuario inicial con rol administrador ("user: admin | password: admin") y configuración inicial del juego.