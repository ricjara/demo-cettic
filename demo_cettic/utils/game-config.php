<?php
	// Dependencias
	include ('../config/siteconf.php');
	
	// Obtener datos
	$query = "SELECT `player_speed`, `play_time`, `item_score` FROM game_config LIMIT 1";
	$result = mysqli_query($conn, $query);
	
	if($result) {
		$row = mysqli_fetch_assoc($result);
		// Retornar json para lectura desde juego
		echo json_encode($row);
	}
	else {
		echo "Error de conexión.";
	}
	
?>