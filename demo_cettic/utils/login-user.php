<?php
	// Dependencias
	include ('../config/siteconf.php');
	
	//Obtener POST data
	if(isset($_POST["username"]) && isset($_POST["password"]) && !empty($_POST["username"]) && !empty($_POST["password"])) {
		$player_username = $_POST["username"];
		$player_password = $_POST["password"];
	}
	else {
		echo "{\"error\":\"4\",\"userId\":\"0\"}";
		exit();
	}

	// Obtener datos de usuario para comparar
	$statement = $conn->prepare("SELECT `id`, `password` FROM `users` WHERE `username`=?");
	$statement->bind_param("s", $player_username);
	if($statement->execute()) {
		//si se encuentran resultados
		$result = $statement->get_result();
		if($result->num_rows > 0) {
			$row = $result->fetch_assoc();
			// Comparar contraseña con hash
			if (password_verify($player_password, $row["password"])) {
				echo "{\"error\":\"0\",\"userId\":\"".$row['id']."\"}";
			}
			else {
				echo "{\"error\":\"1\",\"userId\":\"0\"}";
			}
		}
		else {
			echo "{\"error\":\"2\",\"userId\":\"0\"}";
		}
		
	}
	else {
		echo "{\"error\":\"3\",\"userId\":\"0\"}";
	}
	//Cerrar conexión
	$statement->close();
	$conn->close();
	
	/* --- Errores --- 
	
		0: No hay error
		1: Credenciales no coinciden
		2: No se encontraron resultados
		3: Error desconocido
		4: Error de request
		
	*/
?>