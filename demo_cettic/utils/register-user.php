<?php
	// Dependencias
	include ('../config/siteconf.php');
	
	//Obtener POST data
	if(isset($_POST["username"]) && isset($_POST["password"]) && !empty($_POST["username"]) && !empty($_POST["password"])) {
		$player_username = $_POST["username"];
		$player_password = $_POST["password"];
	}
	else {
		echo "{\"error\":\"3\"}";
		exit();
	}
	
	// Hashear contraseña (método password_hash disponible a partir de PHP 5)
	$pass_hash = password_hash($player_password, PASSWORD_BCRYPT);

	// Insertar datos con sentencias preparadas
	$statement = $conn->prepare("INSERT INTO `users` (`username`, `password`, `role`) VALUES (?, ?, 0)");
	$statement->bind_param("ss", $player_username, $pass_hash);
	if($statement->execute()) {
		echo "{\"error\":\"0\"}";
	}
	else if( $conn->errno == 1062) { // Duplicate key => usuario ya existe
		echo "{\"error\":\"1\"}";
	}
	else {
		echo "{\"error\":\"2\"}";
	}
	//Cerrar conexión
	$statement->close();
	$conn->close();
	
	/* --- Errores --- 
	
		0: No hay error
		1: Usuario existente
		2: Error desconocido
		3: Error de request
	
	*/
?>