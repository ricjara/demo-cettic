<?php
	// Validar que los parámetros post estén definidos y sean numéricos
	if(isset($_POST["matchtime"]) && isset($_POST["playerspeed"]) && isset($_POST["itemscore"]) &&
	   is_numeric($_POST["matchtime"]) && is_numeric($_POST["playerspeed"]) && is_numeric($_POST["itemscore"])) {
		
		// Definir formato numérico
		$config_matchtime = number_format($_POST["matchtime"], 0);
		$config_playerspeed = number_format($_POST["playerspeed"], 2);
		$config_itemscore = number_format($_POST["itemscore"], 0);
		
		// No se utiliza where al ser una tabla de una fila
		$config_statement = $conn->prepare("UPDATE game_config SET player_speed = ?, play_time = ?, item_score = ?");
		$config_statement->bind_param("dii", $config_playerspeed, $config_matchtime, $config_itemscore);

		if ($config_statement->execute()) {
			echo "éxito";
		}
	}
	// Obtener datos para pre-llenado
	$gamedata_query = "SELECT `player_speed`, `play_time`, `item_score` FROM game_config LIMIT 1";
	$gamedata_result = mysqli_query($conn, $gamedata_query);
	
	if($gamedata_result) {
		$gamedata = mysqli_fetch_assoc($gamedata_result);
	}
	else {
		echo "Error de conexión.";
	}
?>