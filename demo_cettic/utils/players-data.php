<?php	
	// Obtener datos
	$query = "SELECT user_id, score, username FROM plays JOIN users ON users.id = plays.user_id ORDER BY users.id, plays.id";
	$result = mysqli_query($conn, $query);
	
	// Buscar listado de jugadores
	$query_players = "SELECT user_id, users.username, COUNT(plays.id) \"matches\" FROM plays JOIN users ON users.id = plays.user_id GROUP BY plays.user_id";
	$result_players = mysqli_query($conn, $query_players);
	
	if($result_players) {
		$player_list = mysqli_fetch_all($result_players, MYSQLI_ASSOC);
	}
	
	// Si se buscan los resultados para un usuario específico
	if(isset($_GET["playerid"])) {
		$statement_user = $conn->prepare("SELECT user_id, score, username FROM plays JOIN users ON users.id = plays.user_id WHERE users.id = ? ORDER BY plays.id");
		$statement_user->bind_param("s", $_GET["playerid"]);
		// Si se encuentran resultados, almacenar
		if($statement_user->execute()) {
			$result_user = $statement_user->get_result();
			if($result_user->num_rows > 0) {
				// Obtener todos los puntajes como arreglo
				$rows_user = $result_user->fetch_all(MYSQLI_ASSOC);
			}
		}
	}
	
	if($result) {
		// Obtener set de datos como array
		$plays = mysqli_fetch_all($result, MYSQLI_ASSOC);
		
		
		// Separar set de datos por usuario
		$plays_byuser = array_group_by($plays, "user_id");
		// Encontrar arreglo más largo
		$maxLen = 0;
		foreach($plays_byuser as $auxPlays) {
			$auxLen = sizeof($auxPlays);
			if($auxLen > $maxLen) {
				$maxLen = $auxLen;
			}
		}
		// Inicializar arreglo con promedio
		$avgScore = array();
		// Recorrer sets de datos y calcular promedio por partida
		for($i = 0; $i<$maxLen; $i++) {
			// suma total de los puntajes
			$auxSum = 0;
			// Cantidad de datos considerados
			$auxCount = 0;
			// Recorrer partidas por jugador
			foreach ($plays_byuser as $auxPlays) {
				// Si el índice existe en el arreglo, considerar el puntaje
				if(sizeof($auxPlays) - 1 >= $i) {
					$auxSum += $auxPlays[$i]["score"];
					$auxCount++;
				}
			}
			// Agregar promedio a set de datos calculados
			$avgScore[] = $auxSum/$auxCount;
		}
		
	}
	else {
		echo "Error de conexión.";
		exit();
	}
	
	/*
	 *
	 * FUNCIÓN PARA AGRUPAR ARREGLOS POR LLAVE
	 *
	 */
	 
	function array_group_by(array $array, $key)
	{
		$func = (!is_string($key) && is_callable($key) ? $key : null);
		$_key = $key;
		// Carga el nuevo arreglo, separando por llave
		$grouped = [];
		foreach ($array as $value) {
			$key = null;
			if (is_callable($func)) {
				$key = call_user_func($func, $value);
			} elseif (is_object($value) && isset($value->{$_key})) {
				$key = $value->{$_key};
			} elseif (isset($value[$_key])) {
				$key = $value[$_key];
			}
			if ($key === null) {
				continue;
			}
			$grouped[$key][] = $value;
		}
		// Construcción de grupo recursiva
		if (func_num_args() > 2) {
			$args = func_get_args();
			foreach ($grouped as $key => $value) {
				$params = array_merge([ $value ], array_slice($args, 2, func_num_args()));
				$grouped[$key] = call_user_func_array('array_group_by', $params);
			}
		}
		return $grouped;
	}
?>