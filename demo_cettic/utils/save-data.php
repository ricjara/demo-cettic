<?php
	// Dependencias
	include ('../config/siteconf.php');
	
	//Obtener POST data
	if(isset($_POST["user_id"]) && isset($_POST["score"]) && !empty($_POST["user_id"]) && !empty($_POST["score"])) {
		$player_id = $_POST["user_id"];
		$player_score = $_POST["score"];
	}
	else {
		echo "{\"error\":\"2\"}";
		exit();
	}

	// Insertar datos con sentencias preparadas
	$statement = $conn->prepare("INSERT INTO `plays` (`score`, `user_id`) VALUES (?, ?)");
	$statement->bind_param("ii", $player_score, $player_id);
	if($statement->execute()) {
		echo "{\"error\":\"0\"}";
	}
	else {
		echo "{\"error\":\"1\"}";
	}
	//Cerrar conexión
	$statement->close();
	$conn->close();
	
	/* --- Errores --- 
	
		0: No hay error
		1: Error desconocido
		2: Error de request
	
	*/
?>