<?php
	// Verificar sesión iniciada
	session_start();
	
	if (isset($_SESSION["active_user"])) {
		$hasAccess = true;
	}
	else {
		$hasAccess = false;
	}
	
	// Envío de request login por POST
	if(isset($_POST["username"]) && isset($_POST["password"])) {
		// Obtener post data
		$admin_username = $_POST["username"];
		$admin_password = $_POST["password"];
		
		// Obtener datos de usuario para comparar
		$statement = $conn->prepare("SELECT `id`, `password` FROM `users` WHERE `username`=? AND `role` = 1");
		$statement->bind_param("s", $admin_username);
		if($statement->execute()) {
			//si se encuentran resultados
			$result = $statement->get_result();
			if($result->num_rows > 0) {
				$row = $result->fetch_assoc();
				// Comparar contraseña con hash
				if (password_verify($admin_password, $row["password"])) {
					$_SESSION["active_user"] = $row["id"];
					$hasAccess = true;
				}
				else {
					echo "Error al iniciar sesión. Compruebe sus credenciales.";
				}
			}
			else {
				echo "Usuario administrador inexistente.";
			}
		}
		else {
			echo "Error de comunicación con fuente de datos.";
		}
	}
?>