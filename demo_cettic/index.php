<?php
	// Importar dependencias
	include ('config/siteconf.php');
	include ('utils/players-data.php');
?>
<!DOCTYPE html>
<html>
	<head>
		<title>Demo CETTIC</title>
		
		<style>
			body {
				font-family: Arial, Helvetica, sans-serif;
			}
			
			h2 {
				text-align: center;
			}
			
			.btn {
				border: none;
				padding: 15px 32px;
				text-align: center;
				text-decoration: none;
				display: inline-block;
				font-size: 16px;
				margin: 4px 2px;
				color: white;
				background-color: #008CBA;
				cursor: pointer;
			}
			table, th, td {
				border: 1px solid black;
				border-collapse: collapse;
			}
		</style>
		
		<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.min.js"></script>
	</head>
	
	<body>
		<h2>
			Demo CETTIC - Datos de jugadores
		</h2>
		<div style="width: 100%; text-align: center;">
			<a class="btn" href="admin.php">Ir a admin</a>
		</div>
		
		<!-- GRÁFICO -->
		<div style="width: 40%; min-width:320px; margin-left:auto; margin-right:auto;">
			<canvas id="average" width="200" height="200"></canvas>
			<?php if(isset($_GET["playerid"])) { ?>
			<button class="btn" style="background-color: #0044BA;" id="showUser">Datos de jugador</button>
			<button class="btn" style="background-color: #33AABA;" id="showComparison">Comparar con promedio</button>
			<?php } ?>
		</div>
		
		<!-- TABLA DE JUGADORES -->
		<div style="width: 100%">
			<h3>Ver datos de jugadores</h3>
			<table>
				<tr>
					<th>Nombre de usuario</th>
					<th>Cantidad de partidas</th>
				</tr>
				<?php foreach($player_list as $player) { ?>
				<tr>
					<td><a href="?playerid=<?php echo $player["user_id"]; ?>"><?php echo $player["username"]; ?></a></td>
					<td><?php echo $player["matches"]; ?></td>
				</tr>
				<?php } ?>
			</table>
		</div>
		
		<script>
		// ########################
		// Implementación Chart.js
		// ########################
		var playerData = "";
		<?php
		// Asignar dataset si hay datos de jugador
		if(isset($_GET["playerid"]) && isset($rows_user)) {
		?>
		playerData = {
					label: '<?php echo $rows_user[0]["username"]; ?>',
					data: [<?php foreach($rows_user as $play_user) { echo $play_user["score"].","; } ?>],
					borderWidth: 1,
					backgroundColor: "#FF000090"
				};
		<?php
		}
		?>
		
		// Set datos generales promedio
		var chartData = [{
					label: 'Puntaje por número de partida',
					data: [<?php echo implode(',', $avgScore); ?>],
					borderWidth: 1,
					backgroundColor: "#008CBA80"
				}];
		
		// Si hay datos de jugador individual, agregar set de datos
		if (playerData != "") {
			chartData.push(playerData);
		}
		
		var showData = chartData;
		
		// Datos de configuración de gráfico
		var graphConfig = {
			type: 'line',
			data: {
				// Generar etiquetas a partir de la cantidad de partidas jugadas (máximo)
				labels: [ <?php echo "1"; for($i = 2; $i <= sizeof($avgScore); $i++) { echo ",".$i; } ?>],
				datasets: showData
			},
			options: {
				scales: {
					yAxes: [{
						ticks: {
							beginAtZero:true
						}
					}]
				}
			}
		};
		
		// Crear el gráfico
		var ctx = document.getElementById("average").getContext('2d');
		window.myChart = new Chart(ctx, graphConfig);
		
		// ###################
		// EVENTOS DE BOTONES
		// ###################
		document.getElementById("showUser").addEventListener('click', function() {
			showData = [chartData[1]];
			graphConfig.data.datasets = showData;
			window.myChart.update();
		})
		
		document.getElementById("showComparison").addEventListener('click', function() {
			showData = chartData;
			graphConfig.data.datasets = showData;
			window.myChart.update();
		})
		</script>
	</body>
</html>