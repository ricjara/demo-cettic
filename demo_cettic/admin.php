<?php
	// Importar dependencias
	include('config/siteconf.php');
	include('utils/admin-access.php');
	if ($hasAccess) {
		include('utils/admin-config.php');
	}
	
?>
<!DOCTYPE html>
<html>
	<head>
		<title>Demo CETTIC - Admin</title>
	</head>
	
	<style>
		input {
			width: 300px;
			padding: 12px 20px;
			margin: 8px 0;
			box-sizing: border-box;
			border: 1px solid #555;
			outline: none;
		}
		input:focus {
			background-color: lightblue;
		}
		
		button {
			border: none;
			color: white;
			padding: 12px 28px;
			cursor: pointer;
			background-color: #2196F3;
		}
		
		button:hover {
			background: #0b7dda;
		}
	</style>
	
	<body>
		<?php
		// Si no hay sesión activa, mostrar formulario
		if(!$hasAccess) { 
		?>
		
			<div>
				<form method="POST" action="admin.php">
					<fieldset>
						<legend>Inicio de sesión [ Credenciales: admin/admin ]</legend>

						<input type="text" name="username" placeholder="Nombre de usuario">
						<input type="password" name="password" placeholder="Password">

						<button type="submit" class="">Iniciar Sesión</button>
					</fieldset>
				</form>
			</div>
			
		<?php }
		// Si hay sesión activa, mostrar formulario de configuración
		else { ?>
		
			<div>
				<form method="POST" action="admin.php" style="text-align:center">
					<fieldset>
						<legend>Configuración de Juego</legend>
						<label>Tiempo de partida (segundos)</label><br/>
						<input type="number" name="matchtime" value="<?php echo $gamedata["play_time"]; ?>" placeholder="Tiempo de partida"><br/>
						<label>Velocidad de jugador</label><br/>
						<input type="number" step="0.01" name="playerspeed" value="<?php echo number_format($gamedata["player_speed"],2); ?>" placeholder="Velocidad de jugador"><br/>
						<label>Puntaje por item</label><br/>
						<input type="number" name="itemscore" value="<?php echo $gamedata["item_score"]; ?>" placeholder="Puntaje por item"><br/>

						<button type="submit" class="">Guardar</button>
					</fieldset>
				</form>
			</div>
		
		<?php } ?>
		<a href="index.php">Volver a inicio</a>
	</body>
</html>